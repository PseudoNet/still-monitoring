# Still Monitoring

Remote monitoring for distillation process

**Parts List**
* [ESP32-CAM microcontroller](https://s.click.aliexpress.com/e/_9g0DOl)
* [SSD1306 i2c OLED Display](**https://s.click.aliexpress.com/e/_rJ9EV6**)
* [MAX31865 digital converter](https://s.click.aliexpress.com/e/_s5s82c)
* [PT100 temperature probe](https://www.aliexpress.com/item/32867617470.html)
* [USB Type C to TTL serial module](https://s.click.aliexpress.com/e/_9JZzLB)

![](img/stillcam-circuit-diagram-1.0.png)

*Wiring Diagram*



![](img/MAX31865.png)

*MAX31865 Setup*